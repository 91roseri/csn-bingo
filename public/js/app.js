$(document).ready(function() {

	notificationHandler();
	gameTypeForm();
	$(document).on('click', '.delete-form', onSubmitFormAttribute);
});

function onSubmitFormAttribute(e) {
	e.preventDefault();


	if ($(this).data('delete') == true) {

		if (confirm("You are about to permanently delete the selected items.\n'Cancel' to stop, 'OK' to delete.")) {
			$(this).submit();
		}
	}else if($(this).data('action') == true){
		if (confirm("You are about to change the selected items.\n'Cancel' to stop, 'OK' to change.")) {
			$(this).submit();
		}
	}
	else {
		$(this).submit();
	}
}

function notificationHandler(){
	const notification = $('.notification');
	if(notification){
		setTimeout(() => {
			notification.remove();
		}, 3000);
	}

}

function gameTypeForm(){
	const typeSelector = $('.form-type-selector');
	gameFormTypeLogic(typeSelector);
	typeSelector.on('change', gameFormTypeEvent);
	
}
function gameFormTypeEvent(e){
	gameFormTypeLogic($(this));
}
function gameFormTypeLogic(typeSelector){

	if(typeSelector){
		console.log(typeSelector.val());
		if(typeSelector.val() === 'bingo' ){
			$('#game-form-steps').addClass('hide');
			$('#game-form-bingo').removeClass('hide');
		}else{
			$('#game-form-bingo').addClass('hide');
			$('#game-form-steps').removeClass('hide');
		}
	}
}