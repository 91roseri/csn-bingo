-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Värd: roseri.se.mysql.service.one.com:3306
-- Tid vid skapande: 27 okt 2020 kl 13:01
-- Serverversion: 10.3.25-MariaDB-1:10.3.25+maria~bionic
-- PHP-version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databas: `roseri_se_bergom`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `bingo_tiles`
--

CREATE TABLE `bingo_tiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `game_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` bigint(20) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `bingo_tiles`
--

INSERT INTO `bingo_tiles` (`id`, `game_id`, `description`, `points`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pausgympat hemma', 0, 0, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(2, 1, 'Hösstädat cykeln/bilen', 0, 1, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(3, 1, 'Promenerat minst 30 min i dåligt väder', 0, 2, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(4, 1, 'Gett beröm eller positiv feedback till någon', 0, 3, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(5, 1, 'Tagit en lunchprommis', 0, 4, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(6, 1, 'Arbetat på annat ställe än jag brukar', 0, 5, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(7, 1, 'Stretchat 10 min innan lunch', 0, 6, '2020-10-22 12:24:40', '2020-10-22 12:24:40'),
(8, 1, ' Cyklat eller gått till affären', 0, 7, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(9, 1, 'Rensat / sorterat skåp / förråd', 0, 8, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(10, 1, 'Sociala-medier-fri-dag', 0, 9, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(11, 1, 'Ta med barn / sambo ut på något kul', 0, 10, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(12, 1, 'Tagit med en kompis på kvälls-promenad', 0, 11, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(13, 1, 'Tränat minst 30 minuter', 0, 12, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(14, 1, 'Tagit en promenad, plockat upp minst 5 skräp', 0, 13, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(15, 1, 'Tagit en morgon-promenad', 0, 14, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(16, 1, 'Städat skrivbordet', 0, 15, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(17, 1, 'Gjort något av äpplen (paj, mos, saft etc)', 0, 16, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(18, 1, 'Gått 10 000 steg en dag', 0, 17, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(19, 1, 'Skickat ett vykort till någon jag tycker om', 0, 18, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(20, 1, 'Testat ett nytt fikaställe', 0, 19, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(21, 1, 'Promenerat minst 30 min i fint väder', 0, 20, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(22, 1, ' Lunch-promenerat', 0, 21, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(23, 1, 'Gått igenom min inkorg och rensat', 0, 22, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(24, 1, 'Ätit en ny frukt eller grönsak', 0, 23, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(25, 1, 'Tagit en lunch-prommis', 0, 24, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(26, 1, 'Fikat/grillat utomhus', 0, 25, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(27, 1, 'Gått en lång- promenad', 0, 26, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(28, 1, 'Skänkt något till Erikshjälpen eller liknande', 0, 27, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(29, 1, 'Rensat kyl / frys', 0, 28, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(30, 1, 'Pausgympat hemma', 0, 29, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(31, 1, 'Höstfixat ute / balkong / altan', 0, 30, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(32, 1, 'Stretchat 10 minuter på eftermiddagen', 0, 31, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(33, 1, 'Registrerat mig på bingo.roseri.se', 0, 32, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(34, 1, 'Tagit ut hela friskvårds-timmen', 0, 33, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(35, 1, 'Ätit en vegetarisk måltid', 0, 34, '2020-10-22 13:02:55', '2020-10-22 13:02:55'),
(36, 1, 'Rensat undan jackor och skor i hallen', 0, 35, '2020-10-22 13:02:55', '2020-10-22 13:02:55');

-- --------------------------------------------------------

--
-- Tabellstruktur `games`
--

CREATE TABLE `games` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` text COLLATE utf8_unicode_ci NOT NULL,
  `end_date` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `games`
--

INSERT INTO `games` (`id`, `type`, `title`, `description`, `start_date`, `end_date`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'bingo', 'Höstbingo 2020', NULL, '20201001', '20201031', 2, '2020-10-22 12:24:40', '2020-10-22 13:36:27'),
(2, 'steps', 'Stegtävling höst 2020', NULL, '20201001', '20201031', 2, '2020-10-22 13:22:01', '2020-10-22 13:36:48');

-- --------------------------------------------------------

--
-- Tabellstruktur `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2020_10_16_171351_create_games_table', 1),
('2020_10_16_171542_create_bingo_tiles_table', 1),
('2020_10_16_171630_create_user_steps_table', 1),
('2020_10_16_171659_create_user_bingo_tiles_table', 1);

-- --------------------------------------------------------

--
-- Tabellstruktur `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Erik Rosberg', 'erik@roseri.se', '$2y$10$nOhDxiNg4IAIEZVqcGsN5eN8XZhSRNHE2EU4bGjPzQWwckX66EtQS', 'OoHbLohwSRCVXSolWKYDlhJVMAXiIjG7xOAsFXnAxaeHAt9FlZPQgqimBAij', '2020-10-22 12:19:32', '2020-10-27 12:47:37'),
(2, 'Erik Frank', 'erik@erikfrank.se', '$2y$10$sNYtEZL3WxZ/2rGm9JDyTua7t1H4fUFeyqShveA1LdAP269yfjBXO', 'GgwIVcMLZCloyn4qICoxgvrndhTePUvWQobSpl03zq9Zxonlt7tbwhxcpZYL', '2020-10-22 12:22:43', '2020-10-22 13:45:26'),
(3, 'Frankie', 'erik@diby.nu', '$2y$10$yCb2G..g4vCODEvsB5Il3uzVnn9Tw/LO1M3TZYV5VdDG56KzhkYIG', 'kVmFQBai1C4a33iZcUQQv3RFE5vtCbslyjWS9VGSRESc3CZiHmbWgb8QfL31', '2020-10-22 13:38:43', '2020-10-23 06:44:24'),
(4, 'Andreas Hansson', 'ulf.andreas.hansson@gmail.com', '$2y$10$ABdJI6QtKmAjWyg.qRdLbu.VyTBi/4lLGwWYEPAx7QrUlAruqDVUq', NULL, '2020-10-22 14:23:25', '2020-10-22 14:23:25'),
(5, 'David', 'david.sellen@csn.se', '$2y$10$sgH.NqvwAweDHqSY3fMF6ubAXfQ9ZpwE/K.zPVeu2NicjOBpS00k.', NULL, '2020-10-22 14:25:56', '2020-10-22 14:25:56'),
(6, 'Ove Lundkvist', 'ove@mac.com', '$2y$10$4Gf4VDEE9/tLCwrvlH109.3qWrHeZZbvrulrtC/6uMTIqsgZC5sje', NULL, '2020-10-22 15:08:18', '2020-10-22 15:08:18'),
(7, 'John Råbom', 'johnrabom@yahoo.se', '$2y$10$T56VuvEbolZ/MOQMiAZuc.wp0VLO/05Fjt/ZqVK/ypbLQ4TNvQOoC', NULL, '2020-10-22 15:29:23', '2020-10-22 15:29:23'),
(8, 'Marcus Tingsek', 'tugggatejp@hotmail.com', '$2y$10$DzCl1FZj8PJpUihHL5WT3eDX88ZwjWvIpsLdMZ.g236T0X571KJ2q', NULL, '2020-10-27 08:55:41', '2020-10-27 08:55:41');

-- --------------------------------------------------------

--
-- Tabellstruktur `user_bingo_tiles`
--

CREATE TABLE `user_bingo_tiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `bingo_tile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `user_bingo_tiles`
--

INSERT INTO `user_bingo_tiles` (`id`, `bingo_tile_id`, `user_id`, `game_id`, `created_at`, `updated_at`) VALUES
(25, 13, 3, 1, '2020-10-22 13:45:48', '2020-10-22 13:45:48'),
(27, 26, 3, 1, '2020-10-22 13:46:02', '2020-10-22 13:46:02'),
(28, 27, 3, 1, '2020-10-22 13:46:06', '2020-10-22 13:46:06'),
(29, 36, 3, 1, '2020-10-22 13:46:09', '2020-10-22 13:46:09'),
(30, 33, 3, 1, '2020-10-22 13:46:12', '2020-10-22 13:46:12'),
(31, 7, 1, 1, '2020-10-22 13:50:22', '2020-10-22 13:50:22'),
(34, 9, 3, 1, '2020-10-22 14:22:58', '2020-10-22 14:22:58'),
(35, 3, 3, 1, '2020-10-22 14:22:59', '2020-10-22 14:22:59'),
(36, 4, 3, 1, '2020-10-22 14:23:00', '2020-10-22 14:23:00'),
(37, 25, 4, 1, '2020-10-22 14:23:57', '2020-10-22 14:23:57'),
(38, 33, 4, 1, '2020-10-22 14:24:05', '2020-10-22 14:24:05'),
(39, 4, 4, 1, '2020-10-22 14:24:20', '2020-10-22 14:24:20'),
(40, 2, 5, 1, '2020-10-22 14:26:23', '2020-10-22 14:26:23'),
(41, 4, 5, 1, '2020-10-22 14:26:30', '2020-10-22 14:26:30'),
(42, 5, 5, 1, '2020-10-22 14:26:35', '2020-10-22 14:26:35'),
(43, 8, 5, 1, '2020-10-22 14:26:52', '2020-10-22 14:26:52'),
(44, 26, 5, 1, '2020-10-22 14:27:31', '2020-10-22 14:27:31'),
(45, 33, 5, 1, '2020-10-22 14:27:46', '2020-10-22 14:27:46'),
(46, 16, 5, 1, '2020-10-22 14:29:43', '2020-10-22 14:29:43'),
(47, 31, 5, 1, '2020-10-22 14:29:56', '2020-10-22 14:29:56'),
(48, 14, 5, 1, '2020-10-22 14:30:06', '2020-10-22 14:30:06'),
(49, 6, 5, 1, '2020-10-22 14:30:09', '2020-10-22 14:30:09'),
(50, 4, 6, 1, '2020-10-22 15:08:36', '2020-10-22 15:08:36'),
(51, 5, 6, 1, '2020-10-22 15:08:38', '2020-10-22 15:08:38'),
(52, 6, 6, 1, '2020-10-22 15:08:39', '2020-10-22 15:08:39'),
(53, 8, 6, 1, '2020-10-22 15:08:43', '2020-10-22 15:08:43'),
(54, 10, 6, 1, '2020-10-22 15:08:48', '2020-10-22 15:08:48'),
(55, 11, 6, 1, '2020-10-22 15:08:53', '2020-10-22 15:08:53'),
(56, 12, 6, 1, '2020-10-22 15:08:56', '2020-10-22 15:08:56'),
(57, 13, 6, 1, '2020-10-22 15:08:58', '2020-10-22 15:08:58'),
(58, 14, 6, 1, '2020-10-22 15:09:00', '2020-10-22 15:09:00'),
(59, 15, 6, 1, '2020-10-22 15:09:04', '2020-10-22 15:09:04'),
(60, 9, 6, 1, '2020-10-22 15:09:10', '2020-10-22 15:09:10'),
(61, 3, 6, 1, '2020-10-22 15:09:12', '2020-10-22 15:09:12'),
(62, 2, 6, 1, '2020-10-22 15:09:13', '2020-10-22 15:09:13'),
(63, 1, 6, 1, '2020-10-22 15:09:15', '2020-10-22 15:09:15'),
(64, 16, 6, 1, '2020-10-22 15:09:18', '2020-10-22 15:09:18'),
(65, 18, 6, 1, '2020-10-22 15:09:19', '2020-10-22 15:09:19'),
(66, 20, 6, 1, '2020-10-22 15:09:27', '2020-10-22 15:09:27'),
(67, 21, 6, 1, '2020-10-22 15:09:29', '2020-10-22 15:09:29'),
(68, 22, 6, 1, '2020-10-22 15:09:31', '2020-10-22 15:09:31'),
(69, 23, 6, 1, '2020-10-22 15:09:35', '2020-10-22 15:09:35'),
(70, 24, 6, 1, '2020-10-22 15:09:37', '2020-10-22 15:09:37'),
(71, 25, 6, 1, '2020-10-22 15:09:38', '2020-10-22 15:09:38'),
(72, 26, 6, 1, '2020-10-22 15:09:42', '2020-10-22 15:09:42'),
(73, 27, 6, 1, '2020-10-22 15:09:44', '2020-10-22 15:09:44'),
(74, 29, 6, 1, '2020-10-22 15:09:48', '2020-10-22 15:09:48'),
(75, 30, 6, 1, '2020-10-22 15:09:50', '2020-10-22 15:09:50'),
(76, 33, 6, 1, '2020-10-22 15:09:55', '2020-10-22 15:09:55'),
(77, 35, 6, 1, '2020-10-22 15:10:00', '2020-10-22 15:10:00'),
(78, 32, 6, 1, '2020-10-22 15:10:03', '2020-10-22 15:10:03'),
(79, 7, 6, 1, '2020-10-22 15:10:24', '2020-10-22 15:10:24'),
(80, 31, 6, 1, '2020-10-22 15:10:31', '2020-10-22 15:10:31'),
(81, 36, 6, 1, '2020-10-22 15:10:34', '2020-10-22 15:10:34'),
(82, 1, 7, 1, '2020-10-22 15:29:32', '2020-10-22 15:29:32'),
(83, 3, 7, 1, '2020-10-22 15:29:36', '2020-10-22 15:29:36'),
(84, 4, 7, 1, '2020-10-22 15:29:46', '2020-10-22 15:29:46'),
(85, 5, 7, 1, '2020-10-22 15:29:48', '2020-10-22 15:29:48'),
(86, 6, 7, 1, '2020-10-22 15:29:52', '2020-10-22 15:29:52'),
(87, 8, 7, 1, '2020-10-22 15:29:59', '2020-10-22 15:29:59'),
(88, 13, 7, 1, '2020-10-22 15:30:17', '2020-10-22 15:30:17'),
(89, 15, 7, 1, '2020-10-22 15:30:24', '2020-10-22 15:30:24'),
(90, 16, 7, 1, '2020-10-22 15:30:32', '2020-10-22 15:30:32'),
(91, 18, 7, 1, '2020-10-22 15:30:38', '2020-10-22 15:30:38'),
(92, 22, 7, 1, '2020-10-22 15:30:51', '2020-10-22 15:30:51'),
(93, 25, 7, 1, '2020-10-22 15:31:05', '2020-10-22 15:31:05'),
(94, 27, 7, 1, '2020-10-22 15:33:22', '2020-10-22 15:33:22'),
(95, 31, 7, 1, '2020-10-22 15:33:33', '2020-10-22 15:33:33'),
(96, 33, 7, 1, '2020-10-22 15:34:42', '2020-10-22 15:34:42'),
(97, 34, 7, 1, '2020-10-22 15:34:48', '2020-10-22 15:34:48'),
(98, 35, 7, 1, '2020-10-22 15:34:55', '2020-10-22 15:34:55'),
(100, 8, 3, 1, '2020-10-22 16:18:12', '2020-10-22 16:18:12'),
(101, 6, 3, 1, '2020-10-23 06:43:40', '2020-10-23 06:43:40'),
(102, 16, 3, 1, '2020-10-23 06:43:58', '2020-10-23 06:43:58'),
(103, 33, 8, 1, '2020-10-27 08:56:08', '2020-10-27 08:56:08'),
(104, 29, 8, 1, '2020-10-27 08:56:18', '2020-10-27 08:56:18'),
(105, 34, 8, 1, '2020-10-27 08:56:34', '2020-10-27 08:56:34'),
(106, 35, 8, 1, '2020-10-27 08:56:36', '2020-10-27 08:56:36'),
(107, 36, 8, 1, '2020-10-27 08:56:37', '2020-10-27 08:56:37'),
(108, 32, 8, 1, '2020-10-27 08:56:39', '2020-10-27 08:56:39'),
(109, 31, 8, 1, '2020-10-27 08:56:40', '2020-10-27 08:56:40');

-- --------------------------------------------------------

--
-- Tabellstruktur `user_steps`
--

CREATE TABLE `user_steps` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `steps` bigint(20) NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumpning av Data i tabell `user_steps`
--

INSERT INTO `user_steps` (`id`, `user_id`, `game_id`, `steps`, `date`, `created_at`, `updated_at`) VALUES
(7, 1, 2, 1000, '20201001', '2020-10-22 13:56:04', '2020-10-22 13:56:04'),
(8, 3, 2, 6384, '20201001', '2020-10-22 14:21:35', '2020-10-22 14:21:35'),
(9, 6, 2, 8119, '20201001', '2020-10-22 15:11:50', '2020-10-22 15:11:50'),
(10, 6, 2, 9011, '20201002', '2020-10-22 15:12:28', '2020-10-22 15:12:28'),
(11, 6, 2, 9545, '20201003', '2020-10-22 15:12:57', '2020-10-22 15:12:57'),
(12, 6, 2, 11023, '20201004', '2020-10-22 15:13:17', '2020-10-22 15:13:17'),
(13, 6, 2, 4868, '20201005', '2020-10-22 15:13:49', '2020-10-22 15:13:49'),
(14, 6, 2, 7446, '20201006', '2020-10-22 15:14:28', '2020-10-22 15:14:28'),
(15, 6, 2, 8217, '20201007', '2020-10-22 15:15:45', '2020-10-22 15:15:45'),
(16, 6, 2, 7022, '20201008', '2020-10-22 15:16:33', '2020-10-22 15:16:33'),
(17, 6, 2, 6446, '20201009', '2020-10-22 15:16:58', '2020-10-22 15:16:58'),
(18, 6, 2, 20484, '20201010', '2020-10-22 15:17:45', '2020-10-22 15:17:45'),
(19, 6, 2, 13069, '20201011', '2020-10-22 15:18:10', '2020-10-22 15:18:10'),
(20, 6, 2, 8893, '20201012', '2020-10-22 15:20:36', '2020-10-22 15:20:36'),
(21, 6, 2, 8751, '20201013', '2020-10-22 15:20:58', '2020-10-22 15:20:58'),
(22, 6, 2, 3667, '20201014', '2020-10-22 15:21:17', '2020-10-22 15:21:17'),
(23, 6, 2, 9403, '20201015', '2020-10-22 15:21:41', '2020-10-22 15:21:41'),
(24, 6, 2, 8256, '20201016', '2020-10-22 15:22:12', '2020-10-22 15:22:12'),
(25, 6, 2, 4555, '20201017', '2020-10-22 15:25:38', '2020-10-22 15:25:38'),
(26, 6, 2, 9802, '20201018', '2020-10-22 15:26:01', '2020-10-22 15:26:01'),
(27, 6, 2, 12188, '20201019', '2020-10-22 15:26:27', '2020-10-22 15:26:27'),
(28, 6, 2, 3678, '20201020', '2020-10-22 15:27:06', '2020-10-22 15:27:06'),
(29, 6, 2, 11248, '20201021', '2020-10-22 15:27:27', '2020-10-22 15:33:34'),
(30, 6, 2, 2616, '20201022', '2020-10-23 05:33:06', '2020-10-23 05:33:06'),
(31, 6, 2, 6683, '20201023', '2020-10-26 13:50:16', '2020-10-26 13:50:16'),
(32, 6, 2, 6831, '20201024', '2020-10-26 13:50:35', '2020-10-26 13:50:35'),
(33, 6, 2, 5628, '20201025', '2020-10-26 13:50:56', '2020-10-26 13:50:56');

--
-- Index för dumpade tabeller
--

--
-- Index för tabell `bingo_tiles`
--
ALTER TABLE `bingo_tiles`
  ADD PRIMARY KEY (`id`);

--
-- Index för tabell `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Index för tabell `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index för tabell `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index för tabell `user_bingo_tiles`
--
ALTER TABLE `user_bingo_tiles`
  ADD PRIMARY KEY (`id`);

--
-- Index för tabell `user_steps`
--
ALTER TABLE `user_steps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT för dumpade tabeller
--

--
-- AUTO_INCREMENT för tabell `bingo_tiles`
--
ALTER TABLE `bingo_tiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT för tabell `games`
--
ALTER TABLE `games`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT för tabell `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT för tabell `user_bingo_tiles`
--
ALTER TABLE `user_bingo_tiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT för tabell `user_steps`
--
ALTER TABLE `user_steps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
