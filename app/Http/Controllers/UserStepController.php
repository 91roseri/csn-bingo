<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;
use App\game;
use App\userStep;

class UserStepController extends Controller
{
    //

	public function create(Request $request, $game_id){

		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}

		$game = game::find($game_id);
		if(!$game || $game->type !== 'steps'){
			return redirect()->back();
		}
		$expired = app('App\Http\Controllers\GameController')->isExpired($game, strtotime($request->date), false , '', '');
		if($expired){
			return redirect()->back()->withErrors(['Ogiltigt datum!']);
		}

		if($request->steps < 0){
			return redirect()->back();
		}

		$userStep = userStep::where('game_id', $game_id)->where('user_id', $user->id)->where('date', date('Ymd', strtotime($request->date)))->first();
		if(!$userStep){
			$userStep = new userStep();	
		}
	
		$userStep->game_id = $game_id;
		$userStep->user_id = $user->id;
		$userStep->date = date('Ymd', strtotime($request->date));
		
		$userStep->steps = $request->steps;

		$userStep->save();
		return redirect()->back();
	}
}
