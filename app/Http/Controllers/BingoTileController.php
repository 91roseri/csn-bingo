<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;

use App\userBingoTile;
use App\bingoTile;
use App\game;


class BingoTileController extends Controller
{

	public function toggle($tile_id, $user_id){

		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}
		if($user->id != $user_id){
			return redirect()->back();
		}
		$bingoTile = bingoTile::find($tile_id);

		$already_earned = userBingoTile::where('user_id', $user_id)->where('bingo_tile_id', $tile_id)->first();
		if($already_earned){
			userBingoTile::where('user_id', $user_id)->where('bingo_tile_id', $tile_id)->forceDelete();
		}else{
			userBingoTile::create([
				'bingo_tile_id' => $tile_id,
				'user_id' => $user_id,
				'game_id' => $bingoTile->game_id
			]);
		}
		return redirect()->back();


	}

	public function edit(Request $request, $id){
		$user = Auth::user();
		if(!$user){
			return redirect()->back();
		}

		$bingo_tile = bingoTile::find($id);
		if(!$bingo_tile){
			return redirect()->back();
		}

		$game = game::find($bingo_tile->game_id);
		if(!$game || $game->created_by !== $user->id){
			return redirect()->back();
		}

		$bingo_tile->title = $request->title;
		$bingo_tile->description = $request->description;
		$bingo_tile->save();
		return redirect()->back();

	}

	public function create(Request $request){
		$user = Auth::user();
		if(!$user){
			return redirect()->back();
		}
		if(!isset($request->game_id)){
			return redirect()->back();
		}

		$game = game::find($request->game_id);
		if(!$game || $game->created_by !== $user->id){
			return redirect()->back();
		}


		$bingo_tile = new bingoTile();
		$bingo_tile->title = $request->title;
		$bingo_tile->description = $request->description;

		$bingo_tile->game_id = $game->id;
		$bingo_tile->save();
		return redirect()->back();
	}


	public function destroy($id){
		$user = Auth::user();
		if(!$user){
			return redirect()->back();
		}

		$bingo_tile = bingoTile::find($id);
		if(!$bingo_tile){
			return redirect()->back();
		}

		$game = game::find($bingo_tile->game_id);
		if(!$game || $game->created_by !== $user->id){
			return redirect()->back();
		}

		$bingo_tile->forceDelete();
		userBingoTile::where('bingo_tile_id', $bingo_tile->id)->forceDelete();
		return redirect()->back();
	}

}
