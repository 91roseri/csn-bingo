<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

use App\game;
use App\bingoTile;
use App\userStep;

use App\userBingoTile;



use App\Http\Requests;

class GameController extends Controller
{
	public function index(){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}
		$active_games = $this->get_active_games();
		$inactive_games = $this->get_inactive_games();


		foreach ($active_games as $key => $games) {
			foreach ($games as $game) {
				if($key === 'steps'){
					$tmp_highscore = $this->get_steps_highscore($game);
				}else{
					$tmp_highscore = $this->get_bingo_highscore($game);

				}
				$game = $this->store_placement_and_score($game, $tmp_highscore, $user);


			}
			
		}

		return view('home', compact('user', 'active_games', 'inactive_games'));
	}

	

	public function show($id = null, $user_id = null){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}

		$showingUser = null;
		if(!$user_id){
			$user_id = $user->id;
		}else{
			$showingUser = User::find($user_id);
		}

		$games = game::orderBy('start_date', 'asc')->get();
		if($id){
			$game = game::find($id);
		}else{
			$game = game::orderBy('start_date', 'desc')->first();

		}

		$inactive_games = $this->get_inactive_games();


		$bingoTiles = bingoTile::where('game_id', $game->id)->orderBy('order', 'asc')->get();
		$earnedTiles = userBingoTile::where('user_id', $user_id)->pluck('bingo_tile_id')->toArray();
		$createdByUser= User::where('id', $game->created_by)->first();

		if($game->type === 'steps'){

			$activeUsersIds= userStep::where('game_id', $game->id)->where('user_id', '!=' , $user_id)->pluck('user_id');
		}else{
			$activeUsersIds= userBingoTile::where('game_id', $game->id)->where('user_id', '!=' , $user_id)->pluck('user_id');	
		}

		$activeUsers = User::whereIn('id', $activeUsersIds)->get();

		$expired = $this->isExpired($game);
		$steps = $this->getSteps($game, $user_id);
		$total_steps = $this->getTotalStepNumber($steps);
		$active_games = $this->get_active_games();


		if($game->type === 'steps'){
			$tmp_highscore = $this->get_steps_highscore($game);
		}else{
			$tmp_highscore = $this->get_bingo_highscore($game);
		}
		
		if($showingUser){
			$game = $this->store_placement_and_score($game, $tmp_highscore, $showingUser);
		}else{
			$game = $this->store_placement_and_score($game, $tmp_highscore, $user);
		}
		
		$game->tiles = bingoTile::where('game_id', $game->id)->get();


		$intro = ['title' => ($showingUser? $showingUser->name : ($game->type === 'bingo' ? 'Bingobango' : 'Fast Feet') ),
		'subTitle' => $game->title, 
		'meta' => [$game->type == 'steps'? 'Steg: '. ($game->score ? $game->score  : 0): 'Klara: '. ($game->score ? $game->score  : 0) . ' av '. count($game->tiles),
		'Placering: '.$game->placement],
		'intro_background_img' => $game->type.'-bg.png'];
		return view('game', compact('user', 'game', 'games', 'bingoTiles', 'earnedTiles','createdByUser', 'expired', 'steps','total_steps','activeUsers', 'showingUser', 'intro', 'active_games', 'inactive_games'));

	}

	public function showForUser($id, $user_id){
		return $this->show($id, $user_id);
	}


	public function form($id = -1){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}
		$form_name = 'forms.game-form';
		$game = game::find($id);
		if($game){

			if($game->created_by != $user->id){
				return redirect()->back();
			}
			if($game->end_date){
				$game->end_date = date('Y-m-d', strtotime($game->end_date));	
			}
			if($game->start_date){
				$game->start_date = date('Y-m-d', strtotime($game->start_date));	
			}
			if($game->last_submittion_date){
				$game->last_submittion_date = date('Y-m-d', strtotime($game->last_submittion_date));	
			}
			
			$bingo_tiles = bingoTile::where('game_id', $game->id)->orderBy('order', 'asc')->get();	
		}else{
			$game = new game();
			$bingo_tiles = [];
		}


		return view('form-view', compact('game', 'user','form_name', 'bingo_tiles'));
	}

	public function create(Request $request){
		$user = Auth::user();
		if(!$user){
			return redirect()->back();
		}
		$game = new game();
		$game->created_by = $user->id;
		if(isset($request->type)){
			$game->type = $request->type;
		}
		$game = $this->storeBasicGameInfo($request, $game);
		$game->save();

		if($game->type === 'bingo'){
			$this->storeBingoTiles($request, $game->id);	
		}

		return redirect('/game/'.$game->id);
	}
	public function edit(Request $request, $id){
		$user = Auth::user();
		if(!$user){
			return redirect()->back();
		}

		$game = game::find($id);
		if($game->created_by !== $user->id){
			return redirect()->back();
		}

		if($game->type === 'bingo'){
			$this->storeBingoTiles($request, $request->game_id);
		}
		$game = $this->storeBasicGameInfo($request, $game);
		$game->save();

		return redirect('/game/'.$game->id);

	}

	public function storeBasicGameInfo(Request $request, $game){
		if(isset($request->title)){
			$game->title = $request->title;
		}
		if($request->start_date){
			$game->start_date = date('Ymd', strtotime($request->start_date));	
		}
		if($request->end_date){
			$game->end_date = date('Ymd', strtotime($request->end_date));	
		}

		if($request->last_submittion_date){
			$game->last_submittion_date = date('Ymd', strtotime($request->last_submittion_date));	

		}else if($request->end_date){
			$game->last_submittion_date = date('Ymd', strtotime($request->end_date));	
		}

		return $game;

	}
	public function getTotalStepNumber($steps){
		$tot = 0;
		foreach ($steps as $key => $step) {
			$tot += $step->steps;
		}
		return $tot;
	}

	public function getSteps($game, $user_id){
		if($game->type !== 'steps'){
			return [];
		}

		return userStep::where('user_id', $user_id)->where('game_id', $game->id)->orderBy('date', 'asc')->get();
	}

	public function isExpired($game, $date = null, $use_last_submittion_date = true, $plus_date = ' + 1 day', $minus_date = ' - 1 day'){
		if(!$date){
			$date = time();
		}
		$expired = false;
		if($game->last_submittion_date && $use_last_submittion_date){
			if($date > strtotime($game->last_submittion_date.  $plus_date) ){
				$expired = true;
			}

		}else if($game->end_date){
			if($date > strtotime($game->end_date.  $plus_date) ){
				$expired = true;
			}
		}

		if(!$expired && $game->start_date){
			if($date < strtotime($game->start_date.  $minus_date) ){
				$expired = true;
			}	
		}
		return $expired;
	}

	public function storeBingoTiles(Request $request, $game_id) {
		$attributes = array();
		foreach ($request->all() as $key => $value) {
			if(strpos($key, 'bingo_tile_') !== false){
				$parts = explode('_',$key);
				$i = $parts[count($parts) - 2];
				$attribute = $parts[count($parts) - 1];

				if($attribute != 'order' && $value){
					$attributes[$i][$attribute]= $value;	
				}


			}
		}

		foreach ($attributes as $order => $values) {
			$bingoTile = bingoTile::where('game_id', $game_id)->where('order', $order)->first();	
			if(!$bingoTile){
				$bingoTile = new bingoTile();
			}
			foreach ($values as $attr => $value) {
				$bingoTile[$attr] = $value;
			}
			$bingoTile->game_id = $game_id;
			$bingoTile->order = $order;
			$bingoTile->save();
		}
	}

	public function get_active_games(){
		$date = date('Ymd');

		$active_bingo_games = game::where('start_date', '<=' , $date)->where('end_date', '>=', $date)->where('type', 'bingo')->orderBy('start_date', 'desc')->get();
		$active_step_games = game::where('start_date', '<=' , $date)->where('end_date', '>=', $date)->where('type', 'steps')->orderBy('start_date', 'desc')->get();
		$active_games = ['bingo' => $active_bingo_games,
		'steps' => $active_step_games];

		foreach ($active_games as $key => $games) {
			foreach ($games as $game) {
				if($game->type){
					$game->tiles = bingoTile::where('game_id', $game->id)->get();
				}
				$game = $this->store_days_left($game, $date);
			}		
		}
		return $active_games;
	}


	public function get_inactive_games(){
		$date = date('Ymd');

		$active_bingo_games = game::where('end_date', '<', $date)->where('type', 'bingo')->orderBy('start_date', 'desc')->get();
		$active_step_games = game::where('end_date', '<', $date)->where('type', 'steps')->orderBy('start_date', 'desc')->get();
		$active_games = ['bingo' => $active_bingo_games,
		'steps' => $active_step_games];
		$found = false;
		foreach ($active_games as $key => $games) {
			foreach ($games as $game) {
				$found = true;
				if($game->type){
					$game->tiles = bingoTile::where('game_id', $game->id)->get();
				}
			}		
		}
		if(!$found){
			$active_games = [];
		}
		return $active_games;

	}

	public function store_days_left($game, $date = null){
		if(!$date){
			$date = date('Ymd');
		}
		$date1 = new \DateTime($game->end_date);  
		$date2 = new \DateTime($date);   
		$diff = $date2->diff($date1)->format("%a");  
		$days = intval($diff);   
		$game->days_left = $days;
		return $game;

	}

	public function get_steps_highscore($game){

		$highscore_steps = [];

		$steps = userStep::where('game_id',$game->id)->get();
		if(count($steps)){
			foreach ($steps as $key => $s) {
				if(!isset($highscore_steps[$s->user_id])){
					$highscore_steps[$s->user_id] = 0;
				}
				$highscore_steps[$s->user_id] = $highscore_steps[$s->user_id] + $s->steps;
			}
		}
		arsort($highscore_steps);
		return $highscore_steps;
	}

	public function get_bingo_highscore($game){

		$highscore_bingo = [];

		$userBingoTitles = userBingoTile::where('game_id',$game->id)->get();

		if(count($userBingoTitles)){
			foreach ($userBingoTitles as $key => $s) {

				if(!isset($highscore_bingo[$s->user_id])){
					$highscore_bingo[$s->user_id] = 0;
				}

				$highscore_bingo[$s->user_id] = $highscore_bingo[$s->user_id] + 1;
			}
		}
		arsort($highscore_bingo);
		return $highscore_bingo;
	}


	public function store_placement_and_score($game, $highscore, $user){
		if(count($highscore)){
			$i = 1;
			foreach ($highscore as $user_id => $score) {
				if($user_id === $user->id){
					$game->placement = $i;
					$game->score = $score;
					break;
				}
				$i++;
			}
		}
		return $game;
	}


	public function highscore(){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}
		$active_games = $this->get_active_games();
		$inactive_games = $this->get_inactive_games();

		$games = $this->store_highscore_game_data($active_games);
		$inactive_games = $this->store_highscore_game_data($inactive_games);
		$show_inactive_games_button = false;
		if($inactive_games){
			$show_inactive_games_button = true;
		}



		return view('highscore', compact('user', 'active_games', 'games', 'inactive_games', 'show_inactive_games_button'));

	}

	public function highscore_expired(){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}
		$active_games = $this->get_inactive_games();

		if( !( (isset($active_games['bingo']) && count($active_games['bingo'])) || ( isset($active_games['steps']) && count($active_games['steps'])))){
			$active_games = false;
		}

		$inactive_games = $this->get_inactive_games();

		$games = $this->store_highscore_game_data($inactive_games);
		$show_inactive_games_button = false;
		return view('highscore', compact('user', 'active_games','games' , 'inactive_games', 'show_inactive_games_button'));


	}

	public function store_highscore_game_data($game_array){
		foreach ($game_array as $type => $games) {

			foreach ($games as $game) {
				if($game->type === 'bingo'){
					$game->highscore = $this->get_bingo_highscore($game);	
				}else{
					$game->highscore = $this->get_steps_highscore($game);	
				}
				
				$new_highscore = [];
				$highscore_user = [];

				if($game->highscore && count($game->highscore)){
					foreach ($game->highscore as $u_id => $points) {

						$tmp_user = User::where('id', $u_id)->first();
						
						if($tmp_user){
							$new_highscore[$tmp_user->name] = $points;
							$highscore_user[] = $tmp_user;
						}
					}
				}

				$game->highscore = $new_highscore;
				$game->highscore_users = $highscore_user;

			}
		}

		return $game_array;
	}

}

