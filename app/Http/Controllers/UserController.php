<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;

use App\Http\Requests;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
	public function show_change_password(){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}

		return view('changepassword', compact('user'));

	}

	public function change_password(Request $request){
		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}

		$rules = array(
			'password' => 'Required|confirmed',
			'new_password' => 'Required|same:new_password2',
			'new_password2' => 'Required',
		);

		$validator = Validator::make($request->all(), $rules);
		if(password_verify($request->password, $user->password) && $request->new_password == $request->new_password2) {
			$user->password = Hash::make($request->new_password);
			$user->save();
			return redirect('/');
		}else{
			return redirect()->back()->withErrors($validator);
		}
	}
}
