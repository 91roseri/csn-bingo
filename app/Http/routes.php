<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'GameController@index')->name('Home');
Route::get('/game/{id}', 'GameController@show')->name('Game');
Route::get('/game/{id}/user/{user_id}', 'GameController@showForUser')->name('Game');

Route::get('/game/create/new', 'GameController@form');
Route::post('/game/create/new', 'GameController@create');
Route::get('/game/edit/{id}', 'GameController@form');
Route::post('/game/edit/{id}', 'GameController@edit');

Route::post('/bingo_tile/edit/{id}', 'BingoTileController@edit');
Route::post('/bingo_tile/create', 'BingoTileController@create');
Route::post('/bingo_tile/delete/{id}', 'BingoTileController@destroy');


Route::post('/steps/add/new/{game_id}', 'UserStepController@create');


Route::get('/highscore', 'GameController@highscore');
Route::get('/highscore/expired', 'GameController@highscore_expired');

Route::get('/changepassword', 'UserController@show_change_password');
Route::post('/changepassword', 'UserController@change_password');


Route::auth();

Route::get('/home', 'HomeController@index');



Route::post('/tile/toggle/{tile_id}/{user_id}', 'BingoTileController@toggle');


