<div class="form" >
	<form  role="form" method="POST" action="{{ url(isset($bingo_tile) && $bingo_tile ? '/bingo_tile/edit/'.$bingo_tile->id : '/bingo_tile/create/') }}"> 
		{{ csrf_field() }}

		<div class="form-group">
			<label for="title" >Titel</label>
			<input id="title" type="text"  name="title" value="{{ old('title', isset($bingo_tile) && $bingo_tile  ? $bingo_tile->title : '') }}" placeholder="Titel">
		</div>


		<div class="form-group">
			<label for="title" >Beskrivning</label>
			<textarea id="description"  name="description" placeholder="Beskrivning...">{{ old('description', isset($bingo_tile) && $bingo_tile  ? $bingo_tile->description : '') }}</textarea>
		</div>
		@if(isset($game))
		<input class="form__element--hidden" id="game_id" type="number" name="game_id" value="{{$game->id}}">
		@endif

		<button type="submit" class="button button--primary">
			{{isset($bingo_tile) && $bingo_tile ? 'Uppdatera' : 'Skapa'}}
		</button>
	</form>

	@if(isset($bingo_tile))
	<form data-action="true"  class="delete-form connect-user__result full-width " method="POST" action="{{ url('/bingo_tile/delete/'.$bingo_tile->id) }}">
		{{ csrf_field() }}
		<button data-action="true" class="button button--secondary" type="submit">Ta bort</button>
	</form>
	@endif

</div>