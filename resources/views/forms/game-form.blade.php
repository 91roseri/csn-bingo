<form class="form" role="form" method="POST" action="{{ $game->id ? url('/game/edit/'.$game->id) : url('/game/create/new')  }}"> 
	{{ csrf_field() }}

	<div class="form-group">
		<label for="title" >Titel</label>
		<input id="title" type="text"  name="title" value="{{ old('title', $game->title) }}" placeholder="Titel" required="true">
	</div>

	@if(!isset($game->id))
	<div class="form-group">
		<label for="type" >Typ</label>
		<select class="form-type-selector" id="type"  name="type" value="{{ old('type', $game->type) }}" placeholder="Typ" required="true">
			<option value="bingo" {{ old('type', $game->type) === 'bingo' ? 'selected' : null }}>Bingo</option>
			<option value="steps" {{ old('type', $game->type) === 'steps' ? 'selected' : null }}>Stegtävling</option>
		</select>
	</div>
	@elseif(isset($game->type))
	<input class="form-type-selector form__element--hidden" value="{{$game->type}}">
	@endif
	<div class="form-group">
		<label for="start_date" >Startdatum</label>
		<input id="start_date" type="date" name="start_date" value="{{ old('start_date', $game->start_date) }}" placeholder="Startdatum" required="true">
	</div>
	<div class="form-group">
		<label for="end_date" >Slutdatum</label>
		<input id="end_date" type="date" name="end_date" value="{{ old('end_date', $game->end_date) }}" placeholder="Slutdatum" required="true">
	</div>

	<div class="form-group">
		<label for="last_submittion_date" >Sista datum för inlämning av resultat</label>
		<input id="last_submittion_date" type="date" name="last_submittion_date" value="{{ old('last_submittion_date', $game->last_submittion_date) }}" placeholder="Sista datum för inlämning av resultat" >
	</div>

	@if(isset($game))
	<input class="form__element--hidden" id="game_id" type="number" name="game_id" value="{{$game->id}}">
	@endif


	


	<div class="margin-top--medium" id="game-form-bingo">
		<h3>Bingobrickor</h3>
		@for ($i = 0; $i < 36; $i++)
		@include('forms.game-bingo-tile-form')
		@endfor
	</div>

	<div id="game-form-steps">
		Steps
	</div>

	<button type="submit" class="button button--primary">
		{{$game->id ? 'Uppdatera' : 'Spara'}}
	</button>
</form>

