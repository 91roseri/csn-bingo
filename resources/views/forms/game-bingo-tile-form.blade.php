
@if(isset($i))
<div class="form" >


	<h2 class="text--light">
		Bingobricka {{$i + 1}}
	</h2>


	<div class="form-group">
		<textarea id="bingo_tile_{{$i}}_description"  name="bingo_tile_{{$i}}_description" placeholder="Beskrivning...">{{ old('bingo_tile_'.$i.'_description', isset($bingo_tiles[$i]) && $bingo_tiles[$i]  ? $bingo_tiles[$i]->description : '') }}</textarea>
	</div>


	@if(isset($i))
	<input class="form__element--hidden" id="bingo_tile_{{$i}}_order" type="number" name="bingo_tile_{{$i}}_order" value="{{$i}}">

	@endif
</div>
@endif