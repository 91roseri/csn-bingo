<form class="text--dark" role="form" method="POST" action="{{ url('/steps/add/new/'.$game->id) }}"> 
	{{ csrf_field() }}

	<div class="form-group text--dark margin-bottom--small">
		<label for="date" >Datum</label>
		<input id="date" type="date"  name="date" value="{{ old('date' ) }}" placeholder="Datum" required="true">
	</div>


	<div class="form-group text--dark margin-bottom--small">
		<label for="steps" >Antal steg</label>
		<input id="steps" type="number"  name="steps" value="{{ old('date' ) }}" placeholder="Antal steg" required="true">

	</div>

	<button type="submit" class=" full-width button button--primary margin-top--medium">
		Registrera steg
	</button>
</form>