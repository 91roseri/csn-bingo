@extends('layouts.app')

@section('content')
<div class="background--dark">
    <div class="container container--max-width padding-top--large">
        <div class="cols cols--justify-center">
            <div class="col col--6 col-md--10 cols cols--gapless cols--justify-center">

                <img style="max-width: 250px;
                margin-bottom: 8rem; width: 50vw; min-height: 150px" src="{{ asset('logo.svg') }}">

                <form class="form-horizontal full-width" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Epostadress</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Lösenord</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group margin-top--small">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Kom ihåg mig
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="button button--secondary margin-top--small full-width">
                                Logga in
                            </button>

                        </div>
                    </div>

                    <div class="form-group">
                        <a class="button button--link margin-top--small  full-width" href="{{ url('/register') }}">Registrera</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
