@extends('layouts.app')

@section('content')
<div class="background--dark padding-header">
	<div class="container container--max-width padding-bottom--large">
		<div class="cols">

			<div class="col col--12">

				@if(isset($games) && ( (isset($games['bingo']) && count($games['bingo'])) || (isset($games['steps']) && count($games['steps']))))
				@foreach($games as $type => $games)
				@foreach($games as $k => $game)
				@if(isset($game->highscore) && count($game->highscore))

				<div class="margin-top--large color--light">

					<div class="highscore__container">
						<h3 class="highscore__title">{{$game->title}}</h3>
					</div>


					@php
					$i = 1;
					@endphp

					@foreach($game->highscore as $user_name => $points)
					<a href="{{url('/game/'. $game->id . '/user/'. $game->highscore_users[$i-1]->id)}}" class="highscore__row {{$i < 4 ? 'large' : ''}} {{$i == 3 ? 'border-bottom' : ''}}">
						<div class="color--light d-flex">
							<span class="highscore__number d-block">
								@if($i ==1)
								<img src="{{ URL::asset('gold-crown.png') }}">

								@elseif($i ==2)
								<img src="{{ URL::asset('silver-crown.png') }}">

								@elseif($i == 3)
								<img src="{{ URL::asset('bronze-crown.png') }}">

								@else
								{{$i}}

								@endif
							</span>
							<span class="highscore__name d-block">{{$user_name}}</span>
						</div>
						<div class="color--light">{{$game->type == 'steps' ? number_format($points, 0, ',', ' ') .' steg' : $points .' av '. count($game->tiles)}}</div>
					</a>

					@php
					$i++;
					@endphp
					@endforeach


				</div>
<!--
				<div class="steps-table steps-table--white  margin-top--large">
					<h2 class="margin-bottom--medium">{{$game->title}}</h2>

					@php
					$i = 1;
					@endphp

					@foreach($game->highscore as $user_name => $points)
					<a href="{{url('/game/'. $game->id . '/user/'. $game->highscore_users[$i-1]->id)}}" class="steps-table__row margin-top--small">
						<div class="steps-table__cell">{{$i}}. {{$user_name}} </div> 
						<div class="steps-table__cell">{{$game->type == 'steps' ? $points .' steg' : $points .' av '. count($game->tiles)}}</div>
					</a>

					@php
					$i++;
					@endphp
					@endforeach


				</div>
			-->

			@endif

			@endforeach
			@endforeach

			@elseif(isset($show_inactive_games_button) && $show_inactive_games_button)
			<h3 class="text--light margin-top--large">Finns inga aktiva listor, vänligen se föregående highscore</h3>

			@endif


		</div>

		@if(isset($show_inactive_games_button) && $show_inactive_games_button )
		<div class="col col--12 margin-top--large">
			<a class="button button--secondary" href="{{url('/highscore/expired')}}">Gå till föregående spel</a>
		</div>
		@endif



	</div>

</div>
</div>



</div>
@endsection


