@extends('layouts.app')

@section('content')

<div class="background--dark padding-header">
	<div class="container container--max-width">
		<div class="cols">

			<div class="col col--12">
				<h1 class="color--light margin-top--medium">Byt lösenord</h1>
				<form class="form-horizontal full-width" role="form" method="POST" action="{{ url('/changepassword') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Gammalt lösenord</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">

							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>


					<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
						<label for="new_password" class="col-md-4 control-label">Nytt lösenord</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="new_password" value="{{ old('new_password') }}">

							@if ($errors->has('new_password'))
							<span class="help-block">
								<strong>{{ $errors->first('new_password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('new_password2') ? ' has-error' : '' }}">
						<label for="new_password2" class="col-md-4 control-label">Upprepa lösenord</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="new_password2" value="{{ old('new_password2') }}">

							@if ($errors->has('new_password2'))
							<span class="help-block">
								<strong>{{ $errors->first('new_password2') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="button button--secondary margin-top--small full-width">
								Spara
							</button>

						</div>
					</div>

				</form>
			</div>

		</div>

	</div>
</div>

@endsection


