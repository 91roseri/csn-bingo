<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CSN Bingo</title>

    <!-- Fonts -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

    <link rel="stylesheet" href="{{ URL::asset('css/app.css')}}" >

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="{{ '/js/app.js'}}"></script>
    <link rel="stylesheet" href="https://use.typekit.net/rgh0qsn.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- reggad på 91roseri@gmail.com -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-J6MR890KKW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-J6MR890KKW');
    </script>

</head>
<body id="app-layout">
    @if (Auth::guest())

    @else


    <nav class="header">
        <div class="container container--max-width">

            <div class="cols">

                <div class="col col--6 header-height cols cols--gapless cols--align-center">

                    <a href="{{ url('/') }}">
                        <img style="max-width: 65px;" src="{{ asset('logo.svg') }}">
                    </a>


                </div>
                <div class="col col--6">
                    <div class="cols cols--gapless cols--justify-end cols--align-center header-height">
                        <div class="col col--dont-grow ">
                            <label for="menu-toggle">                        <img style="max-width: 30px;" src="{{ asset('menu.svg') }}">
                            </label>    
                        </div>
                    </div>
                    
                    <input type="checkbox" class="header__menu-toggle" id="menu-toggle">
                    <div class="header__menu">
                        <label for="menu-toggle" class="header__menu-inner-toggle">Meny</label>
                        
                        <div class="header__menu-inner">
                            <div class="cols cols cols--justify-end cols--gapless">
                                <div class="col col--dont-grow padding-right--medium">
                                    <label tabindex="-1" style="    padding-right: 20px;
                                    margin-top: 14px;
                                    display: block;" for="menu-toggle">Stäng</label>
                                </div>
                            </div>
                            @if (Auth::guest())
                            <ul>

                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Registrera</a></li>
                            </ul>

                            @else

                            <ul class="header__menu-list">
                             @if(isset($active_games) && $active_games)
                             @foreach($active_games as $type => $games)
                             @foreach($games as $type => $game)
                             <li><a  tabindex="-1" href="{{ url('/game/'.$game->id) }}">{{$game->title}}</a></li>
                             @endforeach
                             @endforeach
                             @endif
                             <li><a tabindex="-1" href="{{ url('/highscore') }}">Highscore</a></li>

                             @if(isset($inactive_games) && $inactive_games)
                             <li><a tabindex="-1" href="{{ url('/highscore/expired') }}">Gamla highscore</a></li>

                             @endif

                             <li><a tabindex="-1" href="{{ url('/changepassword') }}">Byt lösenord</a></li>
                             <li><a tabindex="-1" href="{{ url('/logout') }}">Logga ut</a></li>
                             <!--<li><a href="{{ url('/game/create/new') }}">Skapa nytt spel</a></li>-->


                         </ul>
                         @endif


                     </div>
                 </div>
             </div>
         </div>
     </div>
 </nav>

 @endif




 @include('partials.intro')


 @yield('content')

 <!-- JavaScripts -->

 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

 {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
