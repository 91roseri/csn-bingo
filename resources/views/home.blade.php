@extends('layouts.app')

@section('content')
<div class="background--dark padding-header">
    <div class="container container--max-width color--light">
        <div class="cols">
            <div class="col col--12">
                @if(isset($active_games) && ( (isset($active_games['bingo']) && count($active_games['bingo'])) || (isset($active_games['steps']) && count($active_games['steps']))))
                <div class="text--center margin-top">
                    <img style="max-width: 120px; width: 27vw; margin-bottom: 2rem;" src="{{ asset('doggie.png') }}">
                </div>

                <h3 class="text--light text--center  margin-bottom--large">{{$user->name}}</h3>


                <div class="margin-top margin-bottom ">
                    @foreach($active_games as $type => $games)
                    @foreach($games as $k => $game)
                    <a href="{{ url('/game/'.$game->id) }}" class="tile">
                        <div class="cols cols--gapless cols--align-center cols--justify-space">
                            <h2 class="margin-bottom--small">{{$game->title}}</h2>
                            <span class="color--meta">{{$game->days_left}} dagar kvar</span>
                        </div>

                        <div class="cols cols--gapless cols--align-center cols--justify-space">
                            <p class="no-margin margin-bottom--small">{{$game->type == 'steps' ? 'Antal steg' : 'Avklarade utmaningar'}}</p>
                            <span>{{$type == 'steps'? ($game->score ? $game->score  : 0) :  ($game->score ? $game->score  : 0) . '/'. count($game->tiles)}} </span>
                        </div>

                        <div class="cols cols--gapless cols--align-center cols--justify-space">
                            <p class="no-margin">Placering</p>
                            <span>{{$game->placement}}</span>
                        </div>

                    </a>
                    @endforeach
                    @endforeach
                </div>

                @else
                <div style="    min-height: calc(100vh - 70px); display: flex;
    flex-direction: column;">
                    <div class="padding-bottom--small padding-top--small" style="flex: 1 1 auto; display: flex; justify-content: center; align-items: center;">
                        <img  style="max-width: 200px;" src="{{ URL::asset('ingaspel.png') }}">
                    </div>
                    <a class="button button--secondary margin-top--large margin-bottom--large" href="{{url('highscore/expired')}}">Se gamla resultat</a>
                </div>
                @endif




            </div>
        </div>
    </div>
</div>
@endsection
