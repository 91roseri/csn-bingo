@extends('layouts.app')

@section('content')

<div class="background--dark padding-header" > 
	<div class="container container--max-width">
		<div class="cols cols--align-center">
			<div class="col col--8 col-md--12">
				@include($form_name)
			</div>
		</div>
	</div>
</div>
@endsection

