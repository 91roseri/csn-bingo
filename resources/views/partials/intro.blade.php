
@if(isset($intro))
<div class="intro">

	<div class="container container--max-width full-height intro__container">
		<div class="cols cols--align-end full-height intro__cols">
			<div class="col col--12 col--gapless">
				@if(isset($intro['title']))
				<div class=" col  col--12">
					<h1 class="text--light " style="margin-bottom: 10px;">{{$intro['title']}}</h1>
				</div>
				@endif
				@if(isset($intro['subTitle']))
				<div class=" col  col--12 margin-bottom--medium">
					<div class="cols">
						<div class="col col--do-grow">
							<span class="text--light color--meta no-margin">{{$intro['subTitle']}}</span>
						</div>
						@if(isset($intro['meta']) && count($intro['meta']))
						<div class="col col--dont-grow">
							@php
							$i = 0;
							@endphp
							@foreach($intro['meta'] as $meta)
							<span class="{{$i ? 'margin-left--medium': ''}} text--light color--meta">{{$meta}}</span>
							@php
							$i++;
							@endphp
							@endforeach
						</div>

						@endif
					</div>
				</div>
				@endif

			</div>
			
		</div>
		@if(isset($intro['intro_background_img']))
		<img class="intro__background-img" src="{{ asset($intro['intro_background_img']) }}">
		@endif


	</div>
</div>

@endif