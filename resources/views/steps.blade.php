<div class="cols margin-top--medium">
	@if(isset($total_steps))
	<div class="col col--6 col-md--12">
		<h1 class="margin-bottom--small">{{$total_steps}}</h1>
		<p>Antal steg</p>
	</div>
	@endif


	@if(isset($game->placement))
	<div class="col col--6 col-md--12">
		<h1 class="margin-bottom--small">{{$game->placement}}</h1>
		<p>Placering</p>
	</div>
	@endif

</div>










@if(isset($steps) && count($steps))

@php
$data = [];
$labels = [];

foreach ($steps as $key => $s) {
	$data[]  = $s->steps;
	$labels[]  = date('d/m', strtotime($s->date));
}
@endphp


<div class="chart-container margin-top--large">

	<canvas id="myChart" width="400" height="400"></canvas>
</div>
<script>
	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',

		data: {
			labels: <?php echo json_encode($labels); ?>,

			datasets: [{
				fill: false,
				label: 'Antal steg',
				data: <?php echo json_encode($data); ?>,
				borderWidth: 5,
				background: 'transparent',
				borderColor: '#371A45'


			}]

		},

		options: {
			legend: {
				display: false
			},
			maintainAspectRatio: false,
			scales: {
				yAxes: [{
					gridLines: {
						display: true,
						color: "rgba(55,26,69,0.1)"
					},
					ticks: {
						display: false,
					}
				}],
				xAxes: [{
					gridLines: {
						display: false
					}
				}]
			}
		}
	});
</script>


@endif

@if(isset($steps) && count($steps))
<div class="steps-table margin-top--large">
	<h2 class="margin-bottom--medium">Steg</h2>
	@foreach($steps as $step)
	<div class="steps-table__row">
		<div class="steps-table__cell">{{date('Y-m-d', strtotime($step->date))}}</div> <div class="steps-table__cell">{{$step->steps}}</div>
	</div>
	@endforeach
</div>
@endif

@if(!(isset($showingUser) && $showingUser) && !(isset($expired) && $expired))
<div class="margin-top--large">
	@include('forms.steps')
</div>
@endif