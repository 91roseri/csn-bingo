@extends('layouts.app')

@section('content')

<div class="container container--max-width">
	@if(isset($game) && isset($user) && $game->created_by === $user->id)
	<a href="{{url('/game/edit/'. $game->id)}}">Redigera</a>
	@endif

	@if(isset($game) && $game->type === 'bingo')
	@if(isset($bingoTiles) && count($bingoTiles))

	<div class="{{$expired ? 'expired tooltip' : ''}} cols margin-top cols--smaller margin-bottom--large" data-tooltip="Tillgängligt {{$game->start_date}} - {{$game->last_submittion_date}}">

		@foreach($bingoTiles as $tile)
		<div class="col gap col--2 col-md--4">

			@if(!(isset($showingUser) && $showingUser))

			<form 
			style="margin: 0;"
			method="post"
			action="/tile/toggle/{{$tile->id}}/{{$user->id}}">
			{{ csrf_field() }}

			<button class="bingo-tile 
			@if(in_array($tile->id,$earnedTiles)  )
			active
			@endif
			" type="submit"
			href="#"
			><div class="bingo-tile__content"><span>{{$tile->description}}</span></div>
		</button>
	</form>
	@else
	<div class="bingo-tile @if(in_array($tile->id,$earnedTiles)  )
		active
		@endif">
		<div class="bingo-tile__content"><span>{{$tile->description}}</span></div>


	</div>
	@endif
</div>
@endforeach
</div>
</div>
@endif

@else
<div class="container container--max-width">
	<div class="cols">
		<div class="col col--12">
			@include('steps')
		</div>
		@endif
	</div>


</div>

@endsection


