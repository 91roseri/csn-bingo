<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBingoTilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bingo_tiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bingo_tile_id');
            $table->integer('user_id'); 
            $table->integer('game_id');               
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bingo_tiles');
    }
}
