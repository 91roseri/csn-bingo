<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBingoTilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bingo_tiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');       
            $table->longText('description')->nullable();
            $table->bigInteger('points')->default(0);
            $table->integer('order')->default(100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bingo_tiles');
    }
}
