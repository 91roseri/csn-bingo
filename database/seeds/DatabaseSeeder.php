<?php

use Illuminate\Database\Seeder;

use App\User;
use App\game;
use App\bingoTile;
use App\userStep;



class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->users();
    	$this->games();
        $this->userSteps();
        $this->bingoTile();
    }

    public function users(){
    	User::insert([
    		'name' => 'Erik Rosberg',
    		'email' => 'erik@roseri.se',
    		'password' => '$2y$10$fATRS11L4trWiDMKK1mTm.qM5BywChNFVwtHvxQkBbBSr7DB1Wo/y',
    	]); 
        User::insert([
            'name' => 'Isabell Näslund',
            'email' => 'isabell@roseri.se',
            'password' => '$2y$10$fATRS11L4trWiDMKK1mTm.qM5BywChNFVwtHvxQkBbBSr7DB1Wo/y',
        ]); 
    }


    public function games(){
    	game::insert([
    		'id' => 1,
    		'title' => 'Höstbingo 2020',
    		'description' => 'Här spelar vi',
            'start_date' => '20201001',
            'end_date' => '20201031',
            'type' => 'bingo',
            'created_by' => 1
        ]); 
    	game::insert([
    		'id' => 2,
    		'title' => 'Vårbingo 2021',
    		'description' => 'Här spelar vi',
            'start_date' => '20210401',
            'end_date' => '20210427',
            'type' => 'bingo',
            'created_by' => 1
        ]); 

        game::insert([
            'id' => 3,
            'title' => 'Stegtävling 2020',
            'description' => 'Här spelar vi',
            'start_date' => '20201001',
            'end_date' => '20201031',
            'type' => 'steps',
            'created_by' => 1
        ]); 

        game::insert([
            'id' => 4,
            'title' => 'Höstbingo 2019',
            'description' => 'Här spelar vi',
            'start_date' => '20191001',
            'end_date' => '20191031',
            'type' => 'bingo',
            'created_by' => 1
        ]);
    }

    public function userSteps(){

        userStep::insert([
            'id' => 1,
            'user_id' => 1,
            'game_id' => 3,
            'steps' => 20000,
            'date' => '20201018'
        ]); 

        userStep::insert([
            'id' => 2,
            'user_id' => 1,
            'game_id' => 3,
            'steps' => 10000,
            'date' => '20201017'
        ]); 

        userStep::insert([
            'id' => 3,
            'user_id' => 1,
            'game_id' => 3,
            'steps' => 30000,
            'date' => '20201019'
        ]); 

             userStep::insert([
            'id' => 4,
            'user_id' => 2,
            'game_id' => 3,
            'steps' => 20000,
            'date' => '20201018'
        ]); 

    }



    public function bingoTile(){
    	bingoTile::insert([
    		'game_id' => 1,
    		'description' => 'Här spelar vi',
    		'order' => 0
    	]); 
    	bingoTile::insert([
    		'game_id' => 1,
    		'description' => 'Här spelar vi',
    		'order' => 1
    	]); 
    	bingoTile::insert([
    		'game_id' => 1,
    		'description' => 'Här spelar vi',
    		'order' => 2
    	]); 

    	bingoTile::insert([
    		'game_id' => 2,
    		'description' => 'Här spelar vi',
    		'order' => 0
    	]); 
        bingoTile::insert([
            'game_id' => 4,
            'description' => 'Här spelar vi',
            'order' => 0
        ]); 
    }
}
